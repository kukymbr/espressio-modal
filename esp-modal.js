/*!
 * Espressio Modal.
 * jQuery-based modal windows plugins.
 *
 * Copyright © 2015 — 2017 Sergey Basov. All rights reserved.
 * Author: Sergey Basov (https://gitlab.com/kukymbr)
 * License: see LICENSE file in project directory.
 */

"use strict";

/**
 * Default config values
 * @type {Object}
 */
var EspressioModalDefaults = {

	// Instance ID.
	// Set undefined/null to generate random ID.
	id: undefined,

	// Wrapper element additional class
	wrapperClass: null,

	// All elements classes prefix
	classPrefix: 'esp-modal-',

	// Class added to empty title/content/buttons-box
	classEmpty: 'empty',

	// Active modal wrapper class
	classActive: 'active',

	// Class added to parent element then modal is shown
	classParentModalActive: 'espressio-modal-active',

	// Use vertical aligner element
	withAligner: true,

	// Element to place modal in.
	// Set NULL for body.
	parentElem: null,

	// Dialogs buttons default class
	buttonsDefaultClass: 'btn',

	// Buttons default data
	buttonDefault: {

		// Element tag name
		tag: 'a',

		// Button text
		text: null,

		// Attributes
		attribs: {},

		// On-click callback(s)
		onclick: null,

		// Close on click (after callbacks from onclick option)
		closeOnClick: true
	},

	// Default apply button additional class
	btnApplyClass: null,

	// Default cancel button additional class
	btnCancelClass: null,

	// Default OK button text
	btnApplyText: 'OK',

	// Default cancel button text
	btnCancelText: 'Cancel',

	// Remove modal elements on dialog hide
	removeOnHide: true,

	// Remove modal elements on dialog hide after specified timeout.
	// Required to correctly send data from modal forms in Chrome.
	// Set empty to remove immediately.
	removeOnHideTimeout: 500,

	// Add close icon to title.
	// Set TRUE render default element.
	// Set jQuery element for custom button.
	closeIcon: true,

	// Enable console messages
	console: false,

	// Default EspressioModalConfirm options
	confirmOptions: {

		// Input jQuery object (optional)
		input: null,

		// Show input in prompt?
		askValue: true,

		// Default input value
		defaultValue: '',

		// Input name (if input option is not set)
		inputName: 'data',

		// Confirm button options or object
		confirmButton: null,

		// Cancel button options or object
		cancelButton: null
	}
};

/**
 * Espressio Modal window constructor
 * @param {Object} [options]
 * @returns {EspressioModal}
 * @constructor
 */
var EspressioModal = function (options) {

	if (!(this instanceof EspressioModal)) {
		return new EspressioModal(options);
	}

	// Initialize instance options
	this._options = $.extend(
		true,
		{},
		EspressioModalDefaults,
		options
	);

	// Set random instance ID if it is not set in options
	this._id = this.option('id', new Date().getTime() + '' + Math.floor(Math.random() * 10E9));

	// Disable console if no window.console available
	this._options.console = this._options.console && !!window.console;

	if (!this._options.classActive) {
		this._options.classActive = 'active';
	}
};

/**
 * Instance config option
 * @type {Object}
 * @private
 */
EspressioModal.prototype._options = {};

/**
 * Instance ID
 * @type {string}
 * @private
 */
EspressioModal.prototype._id = null;

/**
 * Parent element object
 * @type {jQuery}
 * @private
 */
EspressioModal.prototype._parent = null;

/**
 * Modal elements object
 * @type {Object}
 * @private
 */
EspressioModal.prototype._elements = null;

/**
 * Show modal window
 * @return {EspressioModal}
 */
EspressioModal.prototype.show = function () {

	this._setEmptyState('buttonsBox');

	this.wrapper().addClass(this.option('classActive'));
	if (this.option('classParentModalActive')) {
		this.parent().addClass(this.option('classParentModalActive'));
	}
	this.wrapper().trigger('espModalShow');

	this.i('show');

	return this;
};

/**
 * Hide modal window
 * @return {EspressioModal}
 */
EspressioModal.prototype.hide = function () {

	this._hide();
	this.wrapper().trigger('espModalHide');

	this.i('hide');

	if (this.option('removeOnHide')) {
		var timeout = parseInt(this.option('removeOnHideTimeout', 0));
		if (timeout) {
			var instance = this;
			setTimeout(
				function () {
					instance.remove();
				},
				timeout
			);
		} else {
			this.remove();
		}
	}

	return this;
};

/**
 * Check if modal window is active
 * @return {boolean}
 */
EspressioModal.prototype.isOpen = function () {
	return this.wrapper().hasClass(this.option('classActive'));
};

/**
 * Destruct elements
 * @return {EspressioModal}
 */
EspressioModal.prototype.remove = function () {

	if (this._elements === null) {
		return this;
	}

	this._hide();
	this.wrapper().trigger('espModalRemove');
	this.wrapper().remove();

	this._elements = null;

	this.i('remove');

	return this;
};

/**
 * Set title
 * @param {jQuery|String} title
 * @return {EspressioModal}
 */
EspressioModal.prototype.setTitle = function (title) {

	this.elem('title').html('');

	if (title instanceof jQuery) {
		title.prependTo(this.elem('title'));
	} else {
		this.elem('title').html(title);
	}

	this._setEmptyState('title');

	var close = this.option('closeIcon');
	if (close) {
		if (!(close instanceof jQuery)) {
			close = $(
				'<a class="' + this.c('close-icon') + '">' + (typeof close === 'string' ? close : '') + '</a>'
			);
		} else {
			close = close.clone();
		}
		var instance = this;
		close.click(function (e) {
			e.preventDefault && e.preventDefault();
			instance.hide();
			return false;
		});
		close.appendTo(this.elem('title'));
		this.elem('title').addClass('with-close-icon');
	}

	return this;
};

/**
 * Set content
 * @param {jQuery|string} data
 * @return {EspressioModal}
 */
EspressioModal.prototype.setContent = function (data) {

	if (data instanceof jQuery) {
		data.prependTo(this.elem('content'));
		this._setEmptyState('content');
	} else {
		this.elem('content').html(data);
	}

	return this;
};

/**
 * Set content with stripped tags
 * @param {string} text
 * @return {EspressioModal}
 */
EspressioModal.prototype.setText = function (text) {

	var tmp = $('<div>');
	tmp.html(text);

	this.setContent(tmp.text());

	return this;
};

/**
 * Set buttons
 * @param {Array} buttons
 * @return {EspressioModal}
 */
EspressioModal.prototype.setButtons = function (buttons) {

	if (!$.isArray(buttons)) {
		throw new Error('Expected buttons array, ' + (typeof buttons) + ' given');
	}

	var instance = this;

	this.elem().buttons = null;
	this.elem('buttonsBox').html('');

	$.each(buttons, function(i, btn) {
		instance.setButton(btn);
	});

	if (!buttons.length) {
		this._setEmptyState('buttonsBox');
	}

	return this;
};

/**
 * Add button
 * @param {Object|jQuery} btn
 * @return {EspressioModal}
 */
EspressioModal.prototype.setButton = function (btn) {
	if (!btn) {
		this.setApplyButton();
		return this;
	}

	var obj;
	var instance = this;

	if (btn instanceof jQuery) {
		obj = btn;
	} else {
		btn = this._btn(btn);
		obj = $('<' + btn.tag + '>');
		obj.html(btn.text);
		obj.attr(btn.attribs);
		if (btn.onclick) {
			if (!$.isArray(btn.onclick)) {
				btn.onclick = [btn.onclick];
			}
			$.each(btn.onclick, function (i, handler) {
				if (typeof handler === 'function') {
					obj.click(handler);
				}
			});
		}
		if (btn.closeOnClick) {
			obj.click(function (e) {
				e.preventDefault && e.preventDefault();
				instance.hide();
				return false;
			});
		}
	}

	if (this.option('buttonsDefaultClass')) {
		obj.addClass(this.option('buttonsDefaultClass'));
	}

	obj.appendTo(this.elem('buttonsBox'));
	this.elem().buttons = this.elem('buttonsBox').children();

	this._setEmptyState('buttonsBox');

	return this;
};

/**
 * Add apply button
 * @param {Object} [overrideOptions]
 * @returns {EspressioModal}
 */
EspressioModal.prototype.setApplyButton = function (overrideOptions) {
	this.setButton(this._btn(overrideOptions, 'Apply'));
	return this;
};

/**
 * Add cancel button
 * @param {Object} [overrideOptions]
 * @returns {EspressioModal}
 */
EspressioModal.prototype.setCancelButton = function (overrideOptions) {
	this.setButton(this._btn(overrideOptions, 'Cancel'));
	return this;
};

/**
 * Reset all content
 * @return {EspressioModal}
 */
EspressioModal.prototype.reset = function () {

	this.setTitle('');
	this.setContent('');
	this.setButtons([]);

	this.wrapper().trigger('espModalReset');

	this.i('reset');

	return this;
};

/**
 * Get config option value.
 * If key is defined in config and it isn't equal NULL, its value will be returned.
 * Otherwise the defaultValue will be returned.
 * @param {string} key
 * @param {*} [defaultValue]
 * @returns {*}
 */
EspressioModal.prototype.option = function (key, defaultValue) {
	if (typeof key !== 'string') {
		throw new Error('Key is expected to be a string, ' + (typeof key) + ' given');
	}
	if (typeof this._options[key] !== 'undefined' && this._options[key] !== null) {
		return this._options[key];
	}
	if (typeof defaultValue !== 'undefined') {
		return defaultValue;
	}
	return undefined;
};

/**
 * Get current instance ID
 * @returns {string}
 */
EspressioModal.prototype.id = function () {
	return this._id;
};

/**
 * Get modal elements parent container object
 * @returns {jQuery}
 */
EspressioModal.prototype.parent = function () {
	if (this._parent === null) {
		var elem = this.option('parentElem');
		if (elem) {
			if (elem instanceof jQuery) {
				this._parent = elem;
			} else if (typeof elem === 'string') {
				this._parent = $(elem).first();
				if (!this._parent.is(elem)) {
					throw new Error("Element '" + elem + "' does not exist");
				}
			} else {
				throw new Error(
					'parentElem option must be jQuery element instance or a selector string, ' +
					(typeof elem) + ' given'
				);
			}
		} else {
			this._parent = $('body');
		}
	}
	return this._parent;
};

/**
 * Get wrapper element
 * @returns {jQuery}
 */
EspressioModal.prototype.wrapper = function () {
	return this.elem('wrapper');
};

/**
 * Get instance's elements or one of them if name is defined
 * @param {string} [name]
 * @returns {Object|jQuery}
 */
EspressioModal.prototype.elem = function (name) {
	if (this._elements === null) {
		this._elements = {};
		var id = '#' + this.id();
		this._elements.wrapper = $(id);
		if (!this._elements.wrapper.is(id)) {
			this._createElements();
		}
	}

	if (typeof name === 'undefined') {
		return this._elements;
	}

	if (name in this._elements) {
		return this._elements[name];
	}

	throw new Error('Unknown EspressioModal element: ' + name);
};

/**
 * Get class name with prefix
 * @param {string} className
 * @param {boolean} [returnSelector] true for .class-name
 * @returns {string|null}
 */
EspressioModal.prototype.c = function (className, returnSelector) {
	if (!className) {
		return null;
	}
	var cls = this.option('classPrefix', '') + className;

	if (returnSelector) {
		return '.' + cls;
	}
	return cls;
};

/**
 * Print console message is console is enabled
 * @param {string} msg
 */
EspressioModal.prototype.i = function (msg) {
	if (!this.option('console')) {
		return;
	}
	msg = '[EspressioModal][#' + this.id() + '] ' + msg;
	console.log(msg);
};

/**
 * Hide current wrapper and update parent's active state
 * @returns {EspressioModal}
 * @private
 */
EspressioModal.prototype._hide = function () {
	this.wrapper().removeClass(this.option('classActive'));
	if (this.option('classParentModalActive')) {
		var activeSelector = this.c('wrapper', true) + '.' + this.option('classActive');
		var activeWrappers = this.parent().children(activeSelector);
		if (activeWrappers.length === 0) {
			this.parent().removeClass(this.option('classParentModalActive'));
		}
	}
	return this;
};

/**
 * Get button object
 * @param {Object}[override]
 * @param {Object}[defaults]
 * @returns {Object}
 * @private
 */
EspressioModal.prototype._btn = function (override, defaults) {
	if (typeof defaults === 'undefined') {
		defaults = {};
	} else if (typeof defaults === 'string') {
		var preset = defaults;
		defaults = {};
		switch (preset) {
			case 'Apply':
			case 'Cancel':
				defaults = {
					text: this.option('btn' + preset + 'Text'),
					attribs: {
						'class': this.option('btn' + preset + 'Class')
					}
				};
				if (preset === 'Cancel') {
					defaults.closeOnClick = true;
				}
				break;
			default:
				throw new Error('Unknown button defaults preset: ' + defaults);
		}
	}
	if (typeof override === 'undefined') {
		override = {};
	} else if (typeof override === 'string') {
		override = {
			text: override
		};
	}
	defaults = $.extend(
		true,
		{},
		this.option('buttonDefault'),
		defaults
	);
	if (!defaults.text) {
		defaults.text = this.option('btnApplyText');
	}
	return $.extend(
		true,
		{},
		defaults,
		override
	);
};

/**
 * Create modal elements
 * @private
 */
EspressioModal.prototype._createElements = function () {

	var wrapperClass = this.c('wrapper');

	this._elements.wrapper = $('<div id="' + this.id() + '">');
	this._elements.wrapper.addClass(wrapperClass);
	var addClass = this.option('wrapperClass');
	if (addClass) {
		this._elements.wrapper.addClass(addClass);
	}

	var empty = this.option('classEmpty');
	empty = (empty ? ' ' + empty : '');

	this._elements.overlay = $('<div class="' + this.c('overlay') + '">');
	this._elements.overlay.appendTo(this._elements.wrapper);

	this._elements.box = $('<div class="' + this.c('box') + '">');
	this._elements.box.appendTo(this._elements.wrapper);

	this._elements.title = $('<div class="' + this.c('title') + empty + '">');
	this._elements.title.appendTo(this._elements.box);

	this._elements.content = $('<div class="' + this.c('content') + empty + '">');
	this._elements.content.appendTo(this._elements.box);

	this._elements.buttonsBox = $('<div class="' + this.c('buttons') + empty + '">');
	this._elements.buttonsBox.appendTo(this._elements.box);

	this._elements.aligner = null;
	if (this.option('withAligner')) {
		this._elements.aligner = $('<div class="' + this.c('aligner') + '">');
		this._elements.aligner.appendTo(this._elements.wrapper);
	}

	this._elements.buttons = null;

	wrapperClass = '.' + wrapperClass;

	var prevWrapper = this.parent().children(wrapperClass).last();
	if (prevWrapper.is(wrapperClass)) {
		this._elements.wrapper.insertAfter(prevWrapper);
	} else {
		this._elements.wrapper.prependTo(this.parent());
	}

	this.i('elements are created');

	this._setCommonEvents();
};

/**
 * Set common events handlers
 * @private
 */
EspressioModal.prototype._setCommonEvents = function () {

	var instance = this;

	// Close on click on overlay
	this._elements.overlay.click(function () {
		instance.hide();
	});

	// Close on ESC key press
	$('body').keyup(function (e) {
		if (e.keyCode && e.keyCode == 27 && instance.isOpen()) {
			instance.hide();
		}
	});

	this.i('common events handlers are set');
};

/**
 * Add 'empty' class to element if needed
 * @param {string} elemName
 * @private
 */
EspressioModal.prototype._setEmptyState = function (elemName) {
	if (!elemName) {
		throw new Error('Element name is not defined');
	}
	var empty = this.option('classEmpty');
	if (!empty) {
		return;
	}

	var elem = this.elem(elemName);
	var text = $.trim(elem.html());

	if (text === '') {
		elem.addClass(empty);
	} else {
		elem.removeClass(empty);
	}
};



/**
 * EspressioModalPlugin constructor
 * @constructor
 */
var EspressioModalPlugin = function () {
};

/**
 * EspressioModal options object
 * @type {Object}
 * @private
 */
EspressioModalPlugin.prototype._options = {};

/**
 * EspressioModal instance
 * @type {EspressioModal}
 * @private
 */
EspressioModalPlugin.prototype._modal = null;

/**
 * Get EspressioModal instance
 * @return {EspressioModal}
 */
EspressioModalPlugin.prototype.modal = function () {
	if (!this._modal) {
		this._modal = new EspressioModal(this._options);
	}
	return this._modal;
};

/**
 * Set options for EspressioModal instance
 * @param {Object} options
 * @return {EspressioModalPlugin}
 */
EspressioModalPlugin.prototype.setModalOptions = function (options) {
	if (!this._modal) {
		this._options = options;
	} else {
		window.console && console.warn(
			'EspressioModal already initialized for this plugin instance, ' +
			'setModalOptions() has no any effect'
		);
	}
	return this;
};



/**
 * Espressio Modal Form constructor
 * @param {jQuery|string} form
 * @param {string} [title]
 * @param {Array} [buttons]
 * @param {Object} [options]
 * @param {boolean} [showOnInit]
 * @constructor
 */
var EspressioModalForm = function (form, title, buttons, options, showOnInit) {
	if (!form) {
		throw new Error(
			'Form is not defined in EspressioModalForm constructor'
		);
	}

	if (form instanceof jQuery) {
		this._form = form;
	} else if (typeof form === 'string') {
		this._form = $(form);
	} else {
		throw new Error(
			'Expected jQuery form element or its HTML string, ' +
			(typeof form) + ' given'
		);
	}

	var instance = this;

	this.setModalOptions(options);

	this.modal().reset();
	this.modal().setContent(this._form);

	if (title) {
		this.modal().setTitle(title);
	}
	if (buttons) {
		this.modal().setButtons(buttons);
	} else {
		this.modal().setApplyButton(
			{
				onclick: function (e) {
					e && e.preventDefault && e.preventDefault();
					instance.form().submit();
					return false;
				}
			}
		);
		this.modal().setCancelButton();
	}

	// Set focus to first input in form on modal show
	this.modal().wrapper().on('espModalShow', function () {
		instance.form().find(':input:visible').first().focus().select();
	});

	if (showOnInit || typeof showOnInit === 'undefined') {
		this.modal().show();
	}
};

EspressioModalForm.prototype = new EspressioModalPlugin();
EspressioModalForm.prototype.constructor = EspressioModalForm;

/**
 * Form object
 * @type {jQuery}
 * @private
 */
EspressioModalForm.prototype._form = null;

/**
 * Get form element object
 * @returns {jQuery}
 */
EspressioModalForm.prototype.form = function () {
	return this._form;
};



/**
 * Espressio Modal Confirm constructor
 * @param {string} [text]
 * @param {string} [title]
 * @param {Object} [options]          EspressioModalConfirm options
 * @param {Object} [modalOptions]     EspressioModal options
 * @param {boolean} [showOnInit]
 * @constructor
 */
var EspressioModalConfirm = function (text, title, options, modalOptions, showOnInit) {
	this.setModalOptions(modalOptions);
	var modal = this.modal();
	var instance = this;
	modal.reset();

	options = $.extend(
		true,
		{},
		modal.option('confirmOptions'),
		options
	);

	var input = options.input;

	if (input) {
		if (!(input instanceof jQuery)) {
			throw new Error('Expected jQuery element, ' + (typeof input) + ' given');
		}
	} else {
		input = $('<input />');
		input.attr({
			type: (options.askValue ? 'text' : 'hidden'),
			name: options.inputName,
			value: options.defaultValue
		});
	}

	if (!text && options.askValue) {
		text = 'Value: '
	}

	if (typeof title === 'undefined') {
		title = 'Confirm';
	}
	if (title) {
		modal.setTitle(title);
	}

	this._form = $('<form method="POST" class="' + modal.c('confirm-form') + '">');
	this._form.append(
		'<label>' + text + '</label>'
	);

	input.appendTo(this._form);

	this._form.submit(function (e) {
		e.preventDefault && e.preventDefault();
		return false;
	});

	var submit = function (e) {
		e && e.preventDefault && e.preventDefault();

		var fdata = {};
		var data = instance.form().serializeArray();

		$.each(data, function (index, data) {
			fdata[data.name] = instance.form().find('[name="' + data.name + '"]').val();
		});

		instance.form().trigger('confirm', {formData: fdata});
		return false;
	};

	var cancel = function (e) {
		e.preventDefault && e.preventDefault();
		instance.form().trigger('cancel');
		return false;
	};

	input.keyup(function (e) {
		if (e.keyCode == 13) {
			submit();
			instance.modal().hide();
		}
	});

	modal.setContent(this._form);

	modal.setApplyButton($.extend(
		true,
		{},
		options.confirmButton,
		{onclick: submit}
	));
	modal.setCancelButton($.extend(
		true,
		{},
		options.cancelButton,
		{onclick: cancel}
	));

	if (showOnInit || typeof showOnInit === 'undefined') {
		this.modal().show();
	}
};

EspressioModalConfirm.prototype = new EspressioModalPlugin();
EspressioModalConfirm.prototype.constructor = EspressioModalConfirm;

/**
 * Form object
 * @type {jQuery}
 * @private
 */
EspressioModalConfirm.prototype._form = null;

/**
 * Get form element object
 * @returns {jQuery}
 */
EspressioModalConfirm.prototype.form = function () {
	return this._form;
};



/**
 * Show modal alert
 * @param {string} [text]
 * @param {string} [title]
 * @param {jQuery|Object|string} [button]
 * @param {Object} [options]
 * @param {boolean} [showOnInit]
 * @constructor
 */
var EspressioModalAlert = function (text, title, button, options, showOnInit) {
	this.setModalOptions(options);
	this.modal().reset();

	if (text) {
		this.modal().setContent(text);
	}
	if (title) {
		this.modal().setTitle(title);
	}

	if (button) {
		this.modal().setButton(button);
	} else {
		this.modal().setApplyButton();
	}

	if (showOnInit || typeof showOnInit === 'undefined') {
		this.modal().show();
	}
};

EspressioModalAlert.prototype = new EspressioModalPlugin();
EspressioModalAlert.prototype.constructor = EspressioModalAlert;
