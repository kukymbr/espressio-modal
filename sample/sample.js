"use strict";

$(function () {

	// These options will be applied to all instances of EspressioModal* by default
	EspressioModalDefaults.removeOnHide = true;
	EspressioModalDefaults.closeIcon = true;
	EspressioModalDefaults.buttonsDefaultClass = 'btn';
	EspressioModalDefaults.btnApplyClass = 'btn-ok';
	EspressioModalDefaults.btnCancelClass = 'btn-warn';
	EspressioModalDefaults.confirmOptions.askValue = false;
	EspressioModalDefaults.confirmOptions.confirmButton = {text: 'Confirm'};



	// Show custom HTML
	$('#sample-1').click(function () {
		var modal = new EspressioModal();
		modal.setTitle('Lorem Ipsum');
		modal.setContent(
			'<a href="http://en.lipsum.com/feed/html" target="_blank">Lorem ipsum</a> ' +
			'dolor sit amet, consectetur adipiscing elit. ' +
			'Quisque eu nisl in erat fringilla sagittis eu eget dolor. ' +
			'Interdum et malesuada fames ac ante ipsum primis in faucibus. ' +
			'Nam consectetur placerat tortor, vestibulum varius dolor euismod vitae. ' +
			'<strong>Duis ex magna, consequat sit amet scelerisque id, tincidunt at augue.</strong>'
		);
		modal.setButton('Close');
		modal.show();
	});



	// Show form
	$('#sample-2').click(function () {
		var form = $('#sample-2-form').clone();
		form.removeAttr('id');
		new EspressioModalForm(form, 'Sample Form');
	});



	// Show simple confirm
	$('#sample-3').click(function () {
		var confirm = new EspressioModalConfirm('Are you sure you want to confirm this action?');
		confirm.form().on('confirm', function () {
			new EspressioModalAlert('Action confirmed!', 'Well done');
		});
	});



	// Show prompt with value
	$('#sample-4').click(function () {
		var confirm = new EspressioModalConfirm(
			'What is your name?',
			'Hello',
			{
				askValue: true,
				inputName: 'name'
			}
		);
		confirm.form().on('confirm', function (e, d) {
			new EspressioModalAlert(
				'Hello, ' + (d.formData.name ? d.formData.name : 'Human') + '!',
				'Greetings',
				'My name is so beautiful'
			);
		});
	});



	// Show alert
	$('#sample-5').click(function () {
		new EspressioModalAlert('Something somewhere goes wrong', 'Warning', 'Got it!');
	});
});