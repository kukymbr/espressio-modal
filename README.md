# Espressio Modal version 2.0

jQuery-based modal windows plugins.

Check out the [examples page](http://espressio.ru/static/espressio-modal/sample/) to see it in action.

## EspressioModalDefaults

EspressioModalDefaults object allow you override default options in your projects.
Just put something like this somewhere after `esp-modal.js` load:

	EspressioModalDefaults.removeOnHide = true;

Available default values:

	var EspressioModalDefaults = {
    
    	// Instance ID.
    	// Set undefined/null to generate random ID.
    	id: undefined,
    
    	// Wrapper element additional class
    	wrapperClass: null,
    
    	// All elements classes prefix
    	classPrefix: 'esp-modal-',
    
    	// Class added to empty title/content/buttons-box
    	classEmpty: 'empty',
    
    	// Active modal wrapper class
    	classActive: 'active',
    
    	// Class added to parent element then modal is shown
    	classParentModalActive: 'espressio-modal-active',
    
    	// Use vertical aligner element
    	withAligner: true,
    
    	// Element to place modal in.
    	// Set NULL for body.
    	parentElem: null,
    
    	// Dialogs buttons default class
    	buttonsDefaultClass: 'btn',
    
    	// Buttons default data
    	buttonDefault: {
    
    		// Element tag name
    		tag: 'a',
    
    		// Button text
    		text: null,
    
    		// Attributes
    		attribs: {},
    
    		// On-click callback(s)
    		onclick: null,
    
    		// Close on click (after callbacks from onclick option)
    		closeOnClick: true
    	},
    
    	// Default apply button additional class
    	btnApplyClass: null,
    
    	// Default cancel button additional class
    	btnCancelClass: null,
    
    	// Default OK button text
    	btnApplyText: 'OK',
    
    	// Default cancel button text
    	btnCancelText: 'Cancel',
    
    	// Remove modal elements on dialog hide
    	removeOnHide: true,
    
    	// Remove modal elements on dialog hide after specified timeout.
    	// Required to correctly send data from modal forms in Chrome.
    	// Set empty to remove immediately.
    	removeOnHideTimeout: 500,
    
    	// Add close icon to title.
    	// Set TRUE render default element.
    	// Set jQuery element for custom button.
    	closeIcon: true,
    
    	// Enable console messages
    	console: false,
    
    	// Default EspressioModalConfirm options
    	confirmOptions: {
    
    		// Input jQuery object (optional)
    		input: null,
    
    		// Show input in prompt?
    		askValue: true,
    
    		// Default input value
    		defaultValue: '',
    
    		// Input name (if input option is not set)
    		inputName: 'data',
    
    		// Confirm button options or object
    		confirmButton: null,
    
    		// Cancel button options or object
    		cancelButton: null
    	}
    };

## EspressioModal

EspressioModal is the main plugin. All others EspressioModal* plugins use it.
You can use it to show custom content in modal window.

Example:
 
	var modal = new EspressioModal(options);
	modal.setContent('This is my great modal content');
	modal.setTitle('Hello');
	modal.setApplyButton();
	modal.show();

### EspressioModal methods

### new EspressioModal(options)
EspressioModal instance constructor. 
The `options` param is an optional object value which can be set to override EspressioModalDefaults
values for current instance.

#### show()
Show modal window.

#### hide()
Hide modal window. Removes created elements if removeOnHide option is on.

#### isOpen()
Is current instance opened? 

#### remove()
Remove created modal elements.

#### setTitle(title)
Set modal window title. The `title` param may by string or a jQuery object.

#### setContent(data)
Set `data` (string or jQuery instance) as modal window content.

#### setText(text)
Set `text` as modal content without any tags.

#### setButtons(buttons)
Set (and replace previously set) buttons with items of `buttons` array.

#### setButton(btn)
Set button by options object or jQuery instance. 
See `EspressioModalDefaults.buttonDefault` for available button properties.

#### setApplyButton(overrideOptions)
Add Apply button to modal dialog. Its options may by overridden by `overrideOptions` object.

#### setCancelButton(overrideOptions)
Add Cancel button to modal dialog. Its options may by overridden by `overrideOptions` object.

#### reset()
Clear all modal elements contents.

#### option(key, defaultValue)
Get config option value. If `key` is defined in config and it isn't equal `NULL`, its value will be returned. 
Otherwise the `defaultValue` will be returned.

#### id()
Get current instance ID.

#### parent()
Get modal element parent container element (see `EspressioModalDefaults.parentElem`).

#### wrapper()
Get modal wrapper element.

#### elem(name)
If `name` param is empty, object with all modal elements will be returned.
Otherwise, jQuery instance of specified element will be returned.

#### c(className, returnSelector)
Get `className` with prefix from `EspressioModalDefaults.classPrefix`.
If `returnSelector` is true, `.class-name` will be returned with leading dot.

#### i(msg)
Print `msg` to console, if console is enabled (see `EspressioModalDefaults.console`).

### EspressioModal events
When something is happening with EspressioModal instance, 
plugin triggers the event on the `this.wrapper()` element.

Usage example:

	modal.wrapper().on('espModalHide', function() {
		// Do some actions
	});
	
Available events:

#### espModalShow
Triggers each time the EspressioModal.show() is called.

#### espModalHide
Triggers each time the EspressioModal.hide() is called.

#### espModalRemove
Triggers each time the EspressioModal.remove() is called.

#### espModalReset
Triggers each time the EspressioModal.reset() is called.

## EspressioModalForm

Plugin to show custom form in modal window.

Usage example: 

	var form = $('#some-form');
	var modalForm = new EspressioModalForm(form, 'Edit object');
	
	modalForm.form().submit(function() {
		// do some actions
	});
	
### EspressioModalForm methods

#### new EspressioModalForm(form, title, buttons, options, showOnInit)

EspressioModalForm object constructor.

* `form`: jQuery selector / HTML string or jQuery element object.
* `title`: string/jQuery modal window title.
* `buttons`: array of buttons to add. By default — apply & cancel buttons.
* `options`: EspressioModal instance config options.
* `showOnInit`: show after object construction. `TRUE` by default.

#### form()

Get form element jQuery object.

#### modal()

Get current EspressioModal instance.

## EspressioModalConfirm

Plugin to show confirm window.

Usage example:

	var confirm = new EspressioModalConfirm(
		'Are you sure you want to delete this item?',
		'Delete',
		{
			askValue: false
		}
	);
	
	confirm.form().on('confirm', function() {
		// Delete item
	});
	
Usage with askValue enabled:

	var confirm = new EspressioModalConfirm(
        'Please, enter your first name',
        'User info',
        {
            askValue: true,
            inputName: 'firstName'
        }
    );
    
    confirm.form().on('confirm', function(e, d) {
        var userName = d.formData.firstName;
        // Do some actions
    });
    
### EspressioModalConfirm methods
    
#### new EspressioModalConfirm(text, title, options, modalOptions, showOnInit)

EspressioModalConfirm object constructor.

* `text`: confirm text content.
* `title`: string/jQuery modal window title.
* `options`: EspressioModalConfirm options. See `EspressioModalDefaults.confirmOptions` for available properties.
* `modalOptions`: EspressioModal instance config options.
* `showOnInit`: show after object construction. `TRUE` by default.

#### form()

Get form element jQuery object.

#### modal()

Get current EspressioModal instance.

### EspressioModalConfirm events
EspressioModalConfirm triggers two events on `confirm.form()` element:

#### confirm
Triggers when OK button is clicked or form submitted by Enter.

#### cancel
Triggers when modal window is closed without confirmation.

## EspressioModalAlert

Plugin to show modal alert window.

Usage example:

	var alert = new EspressioModalAlert(
		'Item was successfully deleted',
		'Delete'
	);
	
### EspressioModalAlert methods	

#### new EspressioModalAlert(text, title, button, options, showOnInit)
	
EspressioModalAlert object constructor.
	
* `text`: confirm text content.
* `title`: string/jQuery modal window title.
* `button`: button options.
* `options`: EspressioModal instance config options.
* `showOnInit`: show after object construction. `TRUE` by default.

#### modal()

Get current EspressioModal instance.